
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
//originally part of the ScanLine class, i broke it out to make it clean(er)
//This in turn forced me to use generics and (re)learn how to do that.

/**
 * Generic online sorted ArrayList data structure.
 * @author Andrew Baumher
 * @param <T> Generic Object that implements a comparable to itself.
 */
public class SortedList<T extends Comparable<T>>
{

	private final ArrayList<T> theList;

	/**
	 * Default Constructor.
	 */
	SortedList()
	{
		theList = new ArrayList<>();
	}

	/**
	 * Constructor with initial capacity set.
	 * @param capacity initial capacity for internal arraylist.
	 */
	SortedList(int capacity)
	{
		theList = new ArrayList<>(capacity);
	}

	/**
	 * finds where the new element goes in the sorted list, then adds it.
	 * This sorts using brute force, for simplicity's sake.if time constraints
	 * are paramount, merge/heap/quicksort could be used, but i decided that 
	 * was entirely too complicated for so little return.
	 * @param vt the template object to add.
	 */ 
	void add(T vt)
	{
		if (theList.isEmpty())
		{
			theList.add(vt);
		}
		else
		{
			//find if it goes before a given element in the list.
			boolean added = false;
			for (int x = 0; x < theList.size(); x++)
			{
				if (vt.compareTo(theList.get(x)) > 0)
				{
					theList.add(x, vt);
					added = true;
					break;
				}
			}
			//otherwise add it at the end.
			if (!added)
			{
				theList.add(vt);
			}
		}
	}

	/**
	 * Get the sorted list as a sorted queue.
	 * @return the Sorted List as a sorted Queue.
	 */
	Queue<T> getSortedQueue()
	{
		Queue<T> retVal = new LinkedList<>();
		retVal.addAll(theList);
		return retVal;
	}
}
