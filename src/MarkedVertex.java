
/**
 * a comparable integer vertex primarily used for drawing.
 * @author Andrew Baumher
 */
public class MarkedVertex implements Comparable<MarkedVertex>
{
	int x,y;
	boolean isTop, isValid;
	Point[] Spawns;

	/**
	 * Constructor
	 * @param x the x coordinate
	 * @param y the y coordinate
	 */
	MarkedVertex(int x, int y)
	{
		this.x = x;
		this.y = y;
		isValid = true;
	}

	/**
	 * spawners are added to the current point list (for drawing) when this 
	 * vertex is hit. two spawners occur if this is a relative top, one for a side
	 * and none for a bot. horizontal lines have edge case rules, treated in 
	 * separate helper functions.
	 * @param prev the previous vertex to check
	 * @param next the next vertex to check
	 * @return 
	 */
	boolean createSpawners(MarkedVertex prev, MarkedVertex next)
	{
		//rare behavior that only occurs if going from a large clipping area
		//to a very small viewport, or something of the like.
		//input is sanitized to remove this issue otherwise.
		if ((prev.y == y && next.y == y) || this.compareTo(prev) == 0 || this.compareTo(next) == 0)
		{
			isValid = false;
			return false;
		}

		//if both are higher, this is a bot
		if (prev.y > y && next.y >= y)
		{
			Spawns = new Point[0];
			isTop = false;
		}
		//if both are lower, this is a top
		else if (prev.y < y && next.y < y)
		{
			Spawns = new Point[2];
			float prevDx, nextDx;
			prevDx = (x - prev.x) / (y - prev.y);
			nextDx = (x - next.x) / (y - next.y);
			if (prevDx > nextDx)
			{
				Spawns[0] = new Point(this, prev);
				Spawns[1] = new Point(this, next);
			}
			else
			{
				Spawns[0] = new Point(this, next);
				Spawns[1] = new Point(this, prev);
			}
			isTop = true;
		}
		
		//if the previous is equal to this, we are taking care of our hr edge cases
		//two edge cases exist: a step or a plateau. a step is rising(falling)
		//horizontal, then rising(falling). a plateau(or impression, i guess)
		//is rising(falling), horizontal, falling(rising)
		else if (prev.y == y)
		{
			//falling edge. we will try to give the previous edge our spawner
			//if it fails, than this is a plateau, so this is a relative top
			//if it succeeds, this is a step, and thus is not a relative top
			if (next.y < y)
			{
				Spawns = new Point[1];
				Spawns[0] = new Point(this, next);
				isTop = !giveSpawners(prev);
			}
			//rising edge. we will try to take a spawner from the previous edge.
			//if the previous has a spawner to take, this is a step, and the 
			//previous is not a top. if it does not, this is an impression.
			//neither, evidently, are a top.
			else
			{
				Spawns = new Point[0];
				takeSpawners(prev);
				isTop = false;
			}
		}
		//otherwise, this is a case where one is below, one is above
		//or the prev is below, but the next is an even (edge case)
		else
		{
			
			MarkedVertex oppVert = prev.y > y ? next : prev;
			Spawns = new Point[1];
			Spawns[0] = new Point(this, oppVert);
			//checks for edge case. if isTop is now true, it may be a top
			//if false, it is not a top. the next call will determine for
			//sure if this is a top and mark it accordingly.
			isTop = prev.y < y && next.y <= y;
		}
		return true;
	}

	@Override
	/**
	 * A marked vertex is considered greater if it is above or to the equal and
	 * left of the current marked vertex. Generics op!
	 */
	public int compareTo(MarkedVertex o)
	{
		if (y > o.y)
		{
			return 1;
		}
		else if (y == o.y && x < o.x)
		{
			return 1;
		}
		else if (y == o.y && x == o.x)
		{
			return 0;
		}
		return -1;
	}

	/**
	 * Helper that tries to give this vertex's spawner to the prev. 
	 * if the prev has a spawner, it will fail.
	 * @param previous the previous vertex.
	 * @return true if previous takes the spawner, false otherwise
	 */
	private boolean giveSpawners(MarkedVertex previous)
	{
		if (previous.Spawns != null && previous.Spawns.length > 0)
		{
			return false;
		}
		else
		{
			previous.Spawns = new Point[1];
			previous.Spawns[0] = Spawns[0];
			return true;
		}
	}

	/**
	 * Helper that tries to take the spawner from the previous. if the previous
	 * has a spawner and thus thinks it may be a top(plateau), it is corrected
	 * to not be a top, and current takes its spawner. 
	 * @param previous the previous marked vertex to be checked against
	 * @return true if current takes a spawner, false otherwise.
	 */
	private boolean takeSpawners(MarkedVertex previous)
	{
		if (previous.Spawns != null && previous.Spawns.length > 0)
		{
			Spawns = new Point[1];
			Spawns[0] = previous.Spawns[0];
			previous.isTop = false;
			return true;
		}
		return false;
	}
}
