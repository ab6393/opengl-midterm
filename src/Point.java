/**
 * A point on an edge. used for determining where to draw via scanline.
 * @author Andrew Baumher
 */
public class Point
{
	private double deltaX;
	private int y;
	private float x;
	private MarkedVertex bottom;

	/**
	 * Constructor. initializes values
	 * @param start the drawing vertex to start at
	 * @param end the drawing vertex to end at
	 */
	Point(MarkedVertex start, MarkedVertex end)
	{
		x = start.x;
		y = start.y;
		//change in x as y changes by 1
		deltaX = (start.x - end.x * 1.0) / (start.y - end.y);
		bottom = end;
	}
	
	/**
	 * Decrements the x,y values for current scanline draw
	 * @return true if it is valid this draw cycle
	 */
	public boolean decrement()
	{
		y--;
		//if it's off the bottom, it will not be drawn. return false.
		if (y < bottom.y)
		{
			return false;
		}
		
		//if it's at the bottom, see if this edge can be replaced with a new one
		if (y == bottom.y && bottom.Spawns.length > 0)
		{
			//x may jump left or right if there is a horizontal rule here, so update it
			this.x = bottom.Spawns[0].x;
			this.deltaX = bottom.Spawns[0].deltaX;
			this.bottom = bottom.Spawns[0].bottom;	
		}
		//if it's not at the bottom or cannot be replaced, change x as normal
		else
		{
			x -= deltaX;
		}
		return true;
	}

	/**
	 * Returns x as an int for drawing. x is read-only in public context.
	 * @return x, rounded to the nearest integer
	 */
	public int X()
	{
		return (int) Math.round(x);
	}
}