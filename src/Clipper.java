
import Jama.Matrix;
import java.util.LinkedList;

/**
 * Implements a clipping algorithm, given some bounds and a poly to clip.
 * @author Andrew Baumher
 */
public class Clipper
{
	private Vertex ll, ur;

	/**
	 * Constructor - initializes the bounds so that unspecified bounds will not break
	 * the program.
	 */
	public Clipper()
	{
		ll = new Vertex(0,0);
		ur = new Vertex(Float.MAX_VALUE, Float.MAX_VALUE);
	}

	/**
	 * Updates the boundaries with new values
	 * @param left the left bound
	 * @param top the top bound
	 * @param right the right bound
	 * @param bottom the bottom bound
	 */
	public final void setBoundries(float left, float top, float right, float bottom)
	{
		ll = new Vertex(left, bottom);
		ur = new Vertex(right, top);
	}

	/**
	 * Clips a polygon with the previously given bounds and returns 
	 * the polygon as a normalized matrix
	 * @param polygon the polygon to clip
	 * @return the clipped polygon, normalized
	 */
	public Matrix clipPolygon(Matrix polygon)
	{

		LinkedList<Vertex> theList = new LinkedList<>();

		for (int i = 0; i < polygon.getColumnDimension(); i++)
		{
			theList.add(new Vertex((float) polygon.get(0, i), (float) polygon.get(1, i)));
		}
		//run the side checks (LURD order)
		theList = sideCheck(theList, ll.x, new LeftSide());
		theList = sideCheck(theList, ur.y, new TopSide());
		theList = sideCheck(theList, ur.x, new RightSide());
		theList = sideCheck(theList, ll.y, new BottomSide());

		//if the poly lies completely out of the bounds
		if (theList.isEmpty())
		{
			return null;
		}

		double[][] result = new double[3][theList.size()];
		
		//normalizing values
		float shiftX, shiftY, divX, divY;
		shiftX = (ur.x + ll.x) / 2;
		shiftY = (ur.y + ll.y) / 2;
		divX = (ur.x - ll.x) / 2;
		divY = (ur.y - ll.y) / 2;
		
		//input is sanitized, so despite the fact that the list may contain 
		//many vertices, if several adjacent vertices fall on the same horizontal
		//or vertical line, only the outermost two are kept. this shound never occur
		int count = 0;
		for (int x = 0; x < theList.size(); x++)
		{
			int left, right;

			left = x == 0 ? theList.size() - 1 : x - 1;
			right = x == theList.size() - 1 ? 0 : x + 1;

			//vertex cleanup. vertices that lie on the same horizontal or vertical
			//lines as the two adjacent vertices are not added.
			if ((theList.get(left).y != theList.get(x).y || theList.get(x).y != theList.get(right).y) 
			 && (theList.get(left).x != theList.get(x).x || theList.get(x).x != theList.get(right).x))
			{
				float xval, yval;
				xval = (theList.get(x).x - shiftX) / divX;
				yval = (theList.get(x).y - shiftY) / divY;
				result[0][count] = xval;
				result[1][count] = yval;
				result[2][count] = 1;
				count++;
			}
		}
		return new Matrix(result, 3, count);
	}

	/**
	 * finds and replaces vertices outside the current clipping edge
	 *
	 * @param inV incoming vertices
	 * @param checkVal the x or y value to check against
	 * @param c the interface corresponding to the side to be checked.
	 * @return the vertices after clipping along this edge
	 */
	private LinkedList<Vertex> sideCheck(LinkedList<Vertex> inV, float checkVal, comparators c)
	{
		//edge case for empty list. no need to continue if it is empty, simply return
		if (inV.isEmpty())
		{
			return inV;
		}

		//potentially unknown number of vertices. linked list allows for this.
		LinkedList<Vertex> outV = new LinkedList<>();
		//previous vertex, so we can determine the current edge. initialized to last vertex
		Vertex prevVert = inV.getLast();
		//used to store if the previous vertex was out of the clipping edge
		boolean prevOut = c.outsideClipper(checkVal, prevVert);
		for (Vertex v : inV)
		{
			//if current vertex is out of clipping edge
			if (c.outsideClipper(checkVal, v))
			{
				//but the previous one wasn't, we have to replace the current vert
				//with a clipped one.
				if (!prevOut)
				{
					Vertex prev = outV.isEmpty() ? null : outV.getLast();
					//find where it intersects
					Vertex curr = findIntersect(prevVert, v, checkVal, c.isX());
					//if the prev isn't on the clip edge
					if (prev == null || prev.x != curr.x || prev.y != curr.y)
					{
						//add this new point
						outV.add(curr);
					}
				}
				//update previous out to true for next iteration
				prevOut = true;
			}
			else
			{
				//if previous was out (as we are in clipping windows respective to edge)
				if (prevOut)
				{
					Vertex temp = findIntersect(prevVert, v, checkVal, c.isX());
					//add new vertex where edge comes back in, so long as it differs
					//from the current vertex
					if (temp.x != v.x || temp.y != v.y)
					{
						outV.add(temp);
					}
					
				}
				outV.add(v);
				prevOut = false;
			}
			//update previous vertex
			prevVert = v;
		}
		return outV;
	}

	/**
	 * finds where the edge intercepts the clipping edge, then returns that
	 * vertex.
	 *
	 * @param start the Vertex that starts the edge
	 * @param end the Vertex that ends the edge
	 * @param checkVal the value (x or y) of the current clipping edge.
	 * @param isX tells if the check val is x or y
	 * @return the vertex where the edge intersects the check val.
	 */
	private Vertex findIntersect(Vertex start, Vertex end, float checkVal, boolean isX)
	{
		//if the line moves right to left, swap it, to make calculations simpler
		if (start.x > end.x)
		{
			Vertex temp = start;
			start = end;
			end = temp;
		}
		//if this is an x clipping edge (left/right)
		if (isX)
		{
			//find the y value, return that vertex
			float y = ((end.y - start.y) * (checkVal - start.x) / (end.x - start.x)) + start.y;
			return new Vertex(checkVal, y);
		}
		else
		{
			//otherwise, find the x value, return that vertex
			float x = ((end.x - start.x) * (checkVal - start.y) / (end.y - start.y)) + start.x;
			return new Vertex(x, checkVal);
		}
	}

	/**
	 * java version of c/c++ function pointers - interface provides the name and
	 * variables, implementing classes implement the functions.
	 */
	interface comparators
	{

		/**
		 * Determines if the Vertex is outside the clipping window respective to
		 * current edge
		 *
		 * @param checkVal value of the edge we are clipping on
		 * @param vert vertex to be checked
		 * @return true if outside clipping edge, false otherwise
		 */
		boolean outsideClipper(float checkVal, Vertex vert);

		/**
		 * Stores if this is an x edge (left/right) or a y edge (top/bottom)
		 *
		 * @return true if left or right, false otherwise
		 */
		boolean isX();
	}

	/**
	 * Comparator for the left side
	 */
	static class LeftSide implements comparators
	{

		@Override
		public boolean outsideClipper(float checkVal, Vertex vert)
		{
			return vert.x < checkVal;
		}

		@Override
		public boolean isX()
		{
			return true;
		}
	}

	/**
	 * Comparator for the top side
	 */
	static class TopSide implements comparators
	{
		@Override
		public boolean outsideClipper(float checkVal, Vertex vert)
		{
			return vert.y > checkVal;
		}

		@Override
		public boolean isX()
		{
			return false;
		}
	}

	/**
	 * Comparator for the right side
	 */
	static class RightSide implements comparators
	{
		@Override
		public boolean outsideClipper(float checkVal, Vertex vert)
		{
			return vert.x > checkVal;
		}

		@Override
		public boolean isX()
		{
			return true;
		}
	}

	/**
	 * Comparator for the bottom side
	 */
	static class BottomSide implements comparators
	{
		@Override
		public boolean outsideClipper(float checkVal, Vertex vert)
		{
			return vert.y < checkVal;
		}

		@Override
		public boolean isX()
		{
			return false;
		}
	}
}
