
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 * A scanline that could draw multiple, non-overlapping polygons at once -
 * current implementation limits it to one poly at a time, as that is how the
 * given pipeline works.
 */
public class ScanLine
{
	int y, ymin;
	Pipeline pl;
	boolean hasPoly, isLine;
	EdgeCase edgeCase;
	Queue<MarkedVertex> addVerts;
	LinkedList<Point> scanPoints;

	/**
	 * Constructor
	 *
	 * @param p the pipeline to draw on.
	 */
	public ScanLine(Pipeline p)
	{
		addVerts = new LinkedList<>();
		scanPoints = new LinkedList<>();
		pl = p;
		hasPoly = false;
		edgeCase = null;
		isLine = false;
	}

	/**
	 * Pseudo constructor. allows this object to be static. prepares this poly
	 * for drawing
	 *
	 * @param vr vertices of the poly, rounded to nearest integer
	 * @param n number of vertices to check
	 */
	public void newPoly(MarkedVertex[] vr, int n)
	{
		scanPoints = new LinkedList<>();
		addVerts = new LinkedList<>();

		ymin = Integer.MAX_VALUE;
		SortedList<MarkedVertex> toSort = new SortedList<>(n / 2);

		if (n == 1)
		{
			edgeCase = new EdgeCase(vr[0].x, vr[0].x, vr[0].y);
			hasPoly = true;
			return;
		}

		MarkedVertex[] v;
		//edge case for first vertex - rarely hit, but hits hard.
		//algorithm is heavily dependent on previous marked vertex
		//when two have identical y's, it requires knowing the previous'
		//spawners. this isn't a problem, normally, as we just found the
		//previous' spawners. however, this is not the case with the first
		//vertex we check. so we find a pair where the first and last do not
		//have identical ys and shift the array accordingly
		if (vr[n - 1].y == vr[0].y)
		{
			//in incredibly rare instances, we may have all vertices on the 
			//same line. in this case. we simply draw a horizontal line from
			//the min to the max. we store xmin and xmax, in case we need them
			//but they are ignored in every other instance.
			int xmin = vr[0].x, xmax = vr[0].x, iter = n - 1;
			boolean found = false;
			while (iter > 0 && !found)
			{
				xmin = vr[iter].x < xmin ? vr[iter].x : xmin;
				xmax = vr[iter].x > xmax ? vr[iter].x : xmax;
				if (vr[iter - 1].y != vr[iter].y)
				{
					found = true;
				}
				else
				{
					iter--;
				}
			}
			//if we found it, shift the array
			if (found)
			{
				v = new MarkedVertex[vr.length];
				for (int x = 0; x < vr.length; x++)
				{
					v[x] = vr[(x + iter) % vr.length];
				}
			}
			//otherwise, fill the edge case object and we are done.
			else
			{
				edgeCase = new EdgeCase(xmin, xmax, vr[0].y);
				hasPoly = true;
				return;
			}
		}
		else
		{
			v = vr;
		}
		//for input sanitization.
		int firstValid = 0;
		int prev = n - 1;

		//mark and sort loop
		for (int k = 0; k < n; k++)
		{
			ymin = (v[k].y < ymin) ? v[k].y : ymin;
			int next = (k == n - 1) ? firstValid : k + 1;

			//marks current vertex, unless it is invalid
			//only update the previous to this vertex if it
			//is marked as valid
			if (v[k].createSpawners(v[prev], v[next]))
			{
				if (prev >= 0 && v[prev].isTop)
				{
					toSort.add(v[prev]);
				}
				//if this is valid, and we haven't found the first yet,
				//denote the first. takes care of an edge case where we
				//ignore the first as it is identical to the second.
				if (prev < 0)
				{
					firstValid = k;
				}
				prev = k;
			}
		}
		//if the last one is valid and a top, add it.
		//i do not believe this ever gets hit (did in older versions - code cleaup ftw)
		//but i'd rather it be here and unused than missing and needed.
		if (v[n - 1].isValid && v[n - 1].isTop)
		{
			toSort.add(v[n - 1]);
		}

		addVerts = toSort.getSortedQueue();
		scanPoints = new LinkedList<>();
		hasPoly = true;
		//get it ready for drawing
		init();
	}

	/**
	 * helper, initializes the scanline so run will work correctly.
	 */
	private void init()
	{
		//if there's nothing to draw, we're done initializing
		if (addVerts.isEmpty())
		{
			return;
		}
		//otherwise, pop the first vertex in the priority queue.
		//this is the topmost (or tied for topmost) point.
		MarkedVertex tempVert = addVerts.poll();
		y = tempVert.y;
		//add all spawn points to be scanned
		scanPoints.addAll(Arrays.asList(tempVert.Spawns));
		//and keep doing so while other tops exist at this current y
		//note that the priority queue sorts equal 'y's by x ascending
		//so addAll, which appends them to the end, works beautifully.
		//as the scanline also goes from left to right.
		while (!addVerts.isEmpty() && addVerts.peek().y == y)
		{
			tempVert = addVerts.poll();
			scanPoints.addAll(Arrays.asList(tempVert.Spawns));
		}
	}

	/**
	 * fills the polygon
	 */
	public void run()
	{
		//checks if run is called with no poly to draw.
		if (!hasPoly)
		{
			return;
		}
		else if (edgeCase != null)
		{
			pl.drawLine(edgeCase.xLeft, edgeCase.xRight, edgeCase.y);
			return;
		}
		//initial draw, then decrement y.
		draw();
		y--;

		//main run loop. update decrements y on all points to match
		//current y value. they are then drawn, and y is prepared for the
		//next iteration of the y loop.
		while (y >= ymin)
		{
			update();
			draw();
			y--;
		}
	}

	/**
	 * Updates the points to the new y, so drawing can occur.
	 */
	private void update()
	{
		//temporary list allows simultaneous iteration and addition of items 
		LinkedList<Point> tempList = new LinkedList<>();
		//if we dont have any points to add, keep the loop clean. saves time, but looks ugly later
		MarkedVertex toAdd = (!addVerts.isEmpty() && addVerts.peek().y == y) ? addVerts.poll() : null;
		if (toAdd == null)
		{
			for (Point pt : scanPoints)
			{
				if (pt.decrement())
				{
					tempList.add(pt);
				}
			}
		}
		//otherwise, check these. much less "ugly" than before.
		else
		{
			for (Point pt : scanPoints)
			{
				//while we have another point at this y and it is less than the x
				while (toAdd != null && toAdd.x < pt.X())
				{
					tempList.addAll(Arrays.asList(toAdd.Spawns));
					toAdd = (!addVerts.isEmpty() && addVerts.peek().y == y) ? addVerts.poll() : null;
				}
				if (pt.decrement())
				{
					tempList.add(pt);
				}
			}
			//if it is after all the points, loop and add all new verts after this y
			while (toAdd != null)
			{
				tempList.addAll(Arrays.asList(toAdd.Spawns));
				toAdd = (!addVerts.isEmpty() && addVerts.peek().y == y) ? addVerts.poll() : null;
			}
		}
		//update the scanpoints list for drawing
		scanPoints = tempList;
	}

	private void draw()
	{
		//various little errors would trigger this. i believe i squashed them all
		//but if i haven't, well, i haven't.
		if (scanPoints.size() % 2 == 1)
		{
			System.err.println("An error occured while drawing. Testing has\n"
				+ "eliminated all known ways of triggering this. if it still "
				+ "occurs, i guess i'll lose points. let me know of what triggered"
				+ "it so i can fix it for later. Thanks - Andrew");
			System.exit(2);
		}

		//draw a line from every even element (0 indexed) to the next odd element
		Iterator<Point> iter = scanPoints.iterator();
		while (iter.hasNext())
		{
			pl.drawLine(iter.next().X(), iter.next().X(), y);
		}
	}

	/**
	 * Helper class used to treat lines and other such edge cases.
	 */
	private class EdgeCase
	{
		int xLeft, xRight, y;

		/**
		 * Constructor
		 *
		 * @param xl left x coordinate of line to draw
		 * @param xr right x coordinate of line to draw
		 * @param height y coordinate of line to draw
		 */
		EdgeCase(int xl, int xr, int height)
		{
			xLeft = xl;
			xRight = xr;
			y = height;
		}
	}
}
