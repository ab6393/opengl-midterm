//
//  Pipeline.java
//
//  Created by Warren R. Carithers 2016/10/23.
//  Based on code created by Joe Geigel on 1/21/10.
//  Copyright 2016 Rochester Institute of Technology. All rights reserved.
//
//  Contributor:  Andrew Baumher
//

///
// Simple wrapper class for midterm assignment
//
// Only methods in the implementation file whose bodies
// contain the comment
//
//    // YOUR IMPLEMENTATION HERE
//
// are to be modified by students.
///
import Jama.Matrix;
import java.util.ArrayList;

public class Pipeline extends Canvas
{
	final Clipper clip = new Clipper();
	Matrix transformations, viewport, toDraw;
	//add-only list, and only inserts at the end. i suppose a queue would
	//work better.
	ArrayList<Matrix> polys;
	final ScanLine rasterizer = new ScanLine(this);
	///
	// Constructor
	//
	// @param w width of canvas
	// @param h height of canvas
	///

	public Pipeline(int w, int h)
	{
		super(w, h);
		transformations = Matrix.identity(3, 3);
		polys = new ArrayList<>();
	}

	///
	//
	// addPoly - Add a polygon to the canvas.  This method does not draw
	//           the polygon, but merely stores it for later drawing.
	//           Drawing is initiated by the drawPoly() method.
	//
	//           Returns a unique integer id for the polygon.
	//
	// @param p - Array containing the vertices of the polygon to be added.
	// @param n - Number of vertices in polygon
	//
	// @return a unique integer identifier for the polygon
	///
	public int addPoly(Vertex p[], int n)
	{
		double[][] matrixInitializer = new double[3][n];
		for (int i = 0; i < n; i++)
		{
			matrixInitializer[0][i] = p[i].x;
			matrixInitializer[1][i] = p[i].y;
			matrixInitializer[2][i] = 1;
		}
		Matrix newPoly = new Matrix(matrixInitializer, 3, n);
		polys.add(newPoly);
		//guarenteed to be accurate as long as we never remove a poly. 
		return polys.size() - 1;
	}

	///
	// drawPoly - Draw the polygon with the given id.  The polygons hould
	//            be drawn after applying the current transformation to
	//            the vertices of the polygon.
	//
	// @param polyID - the ID of the polygon to be draw.
	///
	public void drawPoly(int polyID)
	{
		toDraw = transformations.times(polys.get(polyID));
		toDraw = clip.clipPolygon(toDraw);
		//if we clip the drawing out of the window, we are done
		if (toDraw == null)
		{
			return;
		}
		toDraw = viewport.times(toDraw);
		//convert to an array of screen coordinates (aligned to the pixel grid)
		int maxSize = toDraw.getColumnDimension();
		MarkedVertex[] verts = new MarkedVertex[maxSize];
		for (int x = 0; x < maxSize; x++)
		{
			verts[x] = new MarkedVertex((int) Math.round(toDraw.get(0, x)), (int) Math.round(toDraw.get(1, x)));
		}
		
		//draw it
		rasterizer.newPoly(verts, verts.length);
		rasterizer.run();
	}

	///
	// clearTransform - sets the current transformation to be the identity 
	///
	public final void clearTransform()
	{
		transformations = Matrix.identity(3, 3);
	}

	///
	//
	// translate - Add a translation to the current transformation by
	//             premultiplying the appropriate translation matrix to
	//             the current transformtion matrix.
	//
	// @param tx - Amount of translation in x.
	// @param ty - Amount of translation in y.
	///
	public void translate(float tx, float ty)
	{
		double[][] toMatrix = {{1, 0, tx}, {0, 1, ty}, {0, 0, 1}};

		Matrix translation = new Matrix(toMatrix);
		transformations = translation.times(transformations);
	}

	///
	//
	// rotate - Add a rotation to the current transformation by premultiplying
	//          the appropriate rotation matrix to the current transformtion
	//          matrix.
	//
	// @param degrees - Amount of rotation in degrees.
	///
	public void rotate(float degrees)
	{
		double rads = (float) Math.toRadians(degrees);
		double[][] toMatrix = {{Math.cos(rads), -Math.sin(rads), 0},
			{Math.sin(rads), Math.cos(rads), 0}, {0, 0, 1}};

		Matrix rotation = new Matrix(toMatrix, 3, 3);
		transformations = rotation.times(transformations);
	}

	///
	// scale - Add a scale to the current transformation by premultiplying
	//         the appropriate scaling matrix to the current transformtion
	//         matrix.
	//
	// @param sx - Amount of scaling in x.
	// @param sy - Amount of scaling in y.
	///
	public void scale(float sx, float sy)
	{
		double[][] toMatrix = {{sx, 0, 0}, {0, sy, 0}, {0, 0, 1}};
		Matrix scale = new Matrix(toMatrix);
		transformations = scale.times(transformations);
	}

	///
	// setClipWindow - Define the clip window.
	//
	// @param bottom - y coord of bottom edge of clip window (in world coords)
	// @param top - y coord of top edge of clip window (in world coords)
	// @param left - x coord of left edge of clip window (in world coords)
	// @param right - x coord of right edge of clip window (in world coords)
	///
	public void setClipWindow(float bottom, float top,
		float left, float right)
	{
		clip.setBoundries(left, top, right, bottom);
	}

	///
	// setViewport - Define the viewport.
	//
	// @param xmin - x coord of lower left of view window (in screen coords)
	// @param ymin - y coord of lower left of view window (in screen coords)
	// @param width - width of view window (in world coords)
	// @param height - width of view window (in world coords)
	///
	public void setViewport(int x, int y, int width, int height)
	{
		double[][] toMatrix = {{width / 2, 0, (x + x + width) / 2},
			{0, height / 2, (y + y + height) / 2}, {0, 0, 1}};
		viewport = new Matrix(toMatrix);
	}

	/**
	 * draws a horizontal line from xLeft to xRight at the current y
	 *
	 * @param xLeft left point on the horizontal line
	 * @param xRight right point on the horizontal line
	 * @param y current y for the horizontal line
	 */
	protected void drawLine(int xLeft, int xRight, int y)
	{
		//note that it will draw a pixel if xLeft == xRight. this is desired.
		for (int x = xLeft; x <= xRight; x++)
		{
			setPixel(x, y);
		}
	}
}