# OpenGL pipeline, done manually.
Implementation of the OpenGL pipeline, from world coordinates to screen coordinates and everything in between. This was done for a Computer Graphics midterm.

## Overview
While most modern rendering APIs will do this kind of rendering for you, it's still important to understand how everything works together so that when you need to get something from world coordinates to screen coordinates, for example, like you would in any application that can be interacted with by the mouse and has a camera, it actually makes sense. This project takes objects that will be rendered, stored as a collection of 2-Dimensional Vectors (from this point on: Vec2s), converts them into a transition matrix, and then displays them with different scales, rotations, translations, viewports, or clipping windows. These different exercises are displayed to a window and can be changed via mouse click.  

This is demonstrated via mouse click events, which change the window to a different set of parameters. The first is 4 objects, unaltered. the second sets the clipping window to clip many of the objects, and also demonstrates the ability to zoom in. the third applies several transformations on the objects, and the fourth shows the same set of objects under 9 different viewports. 

## Implementation
A quick context: the simplest approach to moving verticies uses a different formula than rotating them, which would pose a problem. Fortunately, there is a unified formula, using linear algebra, namely, matrix multiplication. For the sake of simplicity, the JAMA library is used for matrix multiplication. When a clipping window is set, vertices outside the rendering frame are removed from the transition matrix, and replaced with where the lines intersect the edges of the clipping window. Edge cases where the previous and next vertex are also out of bound are checked, and also the edge case where two points are out of bounds but the line between them passes back in. This, to uses mathematics. Viewports are defined and affect the objects via matrix multiplication. Note that in all cases, floating point math is utilized, but then is aligned to the pixel grid.  

Uses JAMA to do matrix multiplication. Also requires jogl and jogamp jars to compile. note that you may need platform specific jars.

## Running
While this is Java, it makes draw calls which at a low level are platform dependant. The code itself is platform-agnostic. If you wish to run this yourself, you will need gluegen-rt and jogl-all jar files, and their respective platform files. note that these are platform dependant, however, if you have the correct platform version, this should run without issue. You can use an IDE or compile this via javac on the command line. 

## Output
In the event you wish to see it working, but don't wish to hunt down the required JAR files, the end results are displayed here, side by side.  
![The output window](img/Run1.png)